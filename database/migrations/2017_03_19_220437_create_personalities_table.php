<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personalities', function (Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('last_name');
            $table->string('social_network_address')->nullable();
            $table->string('artist_work');
            $table->string('work_interpreters');
            $table->string('photo');
            $table->string('photo_mobile');
            $table->integer('profession_id')->unsigned();
            $table->foreign('profession_id')->references('id')->on('profession');
            $table->unique(['name', 'last_name']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personalities');
    }
}
