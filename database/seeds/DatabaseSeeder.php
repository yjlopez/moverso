<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $user = \App\User::create([
            'name' => 'Administrator',
            'email' => 'admin@example.com',
            'last_name' => 'Administrador',
            'password' => Hash::make('Adm1n1strad0r')
        ]);

        $generalConfiguration = \App\Configuration::create([
            'identifier' => 1,
            'configuration' => 'default',
            'video_url' => NULL,
            'welcome_image' => false,
            'seew_video_link' => NULL,
            'peak_video_link' => NULL
        ]);

    }
}
