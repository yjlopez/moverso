<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use \Dimsav\Translatable\Translatable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'news';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'singers', 'premiere_date','see_more_video_link', 'owner_tag', 'photo', 'owner_tag_link'
    ];

    /**
     * The attributes that are translatable
     */
    public $translatedAttributes = ['title, description'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
