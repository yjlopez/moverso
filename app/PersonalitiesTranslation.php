<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonalitiesTranslation extends Model
{
    public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = ['summary'];
}
