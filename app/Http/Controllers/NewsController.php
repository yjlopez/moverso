<?php

namespace App\Http\Controllers;

use App\News;
use App\Profession;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Illuminate\Filesystem\Filesystem;
use DateTime;

class NewsController extends Controller
{
    use AuthenticatesUsers;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $error = '';
        if (Auth::check()){
            $newss = \App\News::all();
            return view('news.index', ['newss' => $newss]);
        }
        return redirect('login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $titles = News::all();
        $fileName = str_random(5).time().'.'.$request->file('news_photo')->guessClientExtension();

        $tomorrow = Carbon::now()->toDateString();

        $this->validate($request, [
            'news_title' => 'required|unique:news_translations,title',
            'news_title_en' => 'required|unique:news_translations,title',
            'news_singers' => 'required',
            'news_description' => 'required',
            'news_description_en' => 'required',
            'news_premiere_date' => 'required|date|after:'.$tomorrow,
            'see_more_link' => 'nullable|url',
            'news_tag_owner' => 'required_with:owner_tag_link',
            'owner_tag_link' => 'nullable|url|required_with:news_tag_owner',
            'news_photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);


        $path = $request->file('news_photo')->storeAs(
            'news-photos', $fileName, 'public'
        );

        $newsToAdd = new News();
        $newsToAdd->translateOrNew('es')->title = $request->news_title;
        $newsToAdd->translateOrNew('en')->title = $request->news_title_en;
        $newsToAdd->singers = $request->news_singers;
        $newsToAdd->translateOrNew('es')->description = $request->news_description;
        $newsToAdd->translateOrNew('en')->description = $request->news_description_en;
        $newsToAdd->premiere_date = $request->news_premiere_date;
        $newsToAdd->see_more_video_link = $request->see_more_link;
        $newsToAdd->owner_tag = $request->news_tag_owner;
        $newsToAdd->owner_tag_link = $request->owner_tag_link;
        $newsToAdd->photo = $fileName;

        $newsToAdd->save();

        return redirect('news');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::find($id);
        return view('news.edit', ['news' => $news]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $newsToEdit = News::find($id);

        $tomorrow = Carbon::now()->toDateString();

        $this->validate($request, [
            'news_title' => 'required|unique:news_translations,title',
            'news_title_en' => 'required|unique:news_translations,title',
            'news_singers' => 'required',
            'news_description' => 'required',
            'news_description_en' => 'required',
            'news_premiere_date' => 'required|date|after:'.$tomorrow,
            'see_more_link' => 'nullable|url',
            'news_tag_owner' => 'required_with:owner_tag_link',
            'owner_tag_link' => 'nullable|url|required_with:news_tag_owner',
            'news_photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);


        if($request->file('news_photo')/*->getClientOriginalName()*/ !== NULL){
            if(file_exists(storage_path('app/public/news-photos/'.$newsToEdit->photo))){
                unlink(storage_path('app/public/news-photos/'.$newsToEdit->photo));
            }

            $fileName = str_random(5).time().'.'.$request->file('news_photo')->guessClientExtension();

            $path = $request->file('news_photo')->storeAs(
                'news-photos', $fileName, 'public'
            );
            $photo = $fileName;
        }else{
            $photo = $newsToEdit->photo;
        }

        $newsToEdit->translate('es')->title = $request->news_title;
        $newsToEdit->translate('en')->title = $request->news_title_en;
        $newsToEdit->singers = $request->news_singers;
        $newsToEdit->translate('es')->description = $request->news_description;
        $newsToEdit->translate('en')->description = $request->news_description;
        $newsToEdit->premiere_date = $request->news_premiere_date;
        $newsToEdit->see_more_video_link = $request->see_more_link;
        $newsToEdit->owner_tag = $request->news_tag_owner;
        $newsToEdit->owner_tag_link = $request->owner_tag_link;
        $newsToEdit->photo = $photo;

        $newsToEdit->save();
        return redirect('news');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @param Request $request
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        $news = News::find($id);
        /*Storage::delete(storage_path('app/public/news-photos/'.$news->photo)); It doesn't work*/
        if(file_exists(storage_path('app/public/news-photos/'.$news->photo))){
            unlink(storage_path('app/public/news-photos/'.$news->photo));
        }
        $news->delete();
        return response("Element deleted.", Response::HTTP_OK);
    }

    public function tiempoTranscurridoFechas($fechaInicio,$fechaFin)
    {
        $fecha = $fechaInicio->diff($fechaFin);
        $tiempo = "";
        $horas = 0;

        //años
        if($fecha->y > 0)
        {
            $tiempo .= $fecha->y;

            if($fecha->y == 1)
                $horas += (365*24);
            else
                $horas += (($fecha->y*365)*24);
        }

        //meses
        if($fecha->m > 0)
        {
            if($fecha->m == 1)
                $horas += 30*24;
            else
                $horas += ($fecha->m*30*24);
        }

        //dias
        if($fecha->d > 0)
        {

            if($fecha->d == 1)
                $horas += 24;
            else
                $horas += ($fecha->d*24);
        }

        //horas
        if($fecha->h > 0)
        {
            $horas += $fecha->h;

            if($horas == 1)
                $tiempo = $horas." hour ago";
            else
                $tiempo = $horas." hours ago";
        }



        return $tiempo;
    }

}
