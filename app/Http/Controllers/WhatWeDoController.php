<?php

namespace App\Http\Controllers;

use App\Whatwedo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class WhatWeDoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $error = '';
        if (Auth::check()){
            $whatwedos = DB::table('what_we_do')->get()->first();
            return view('whatwedo.index', ['whatwedos' => $whatwedos]);
        }
        return redirect('login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('whatwedo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'spanish_description' => 'required|max:480|unique:what_we_do,description_es',
            'english_description' => 'required|max:480|unique:what_we_do,description_en',
        ]);

        $whatwedos = new Whatwedo();

        $whatwedos->description_es = $request->spanish_description;
        $whatwedos->description_en = $request->english_description;

        $whatwedos->save();

        return view('whatwedo.index', ['whatwedos' => $whatwedos]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $whatwedo = Whatwedo::find($id);
        return view('whatwedo.edit', ['whatwedo' => $whatwedo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $whatwedoToEdit = Whatwedo::find($id);

        $this->validate($request, [
            'spanish_description' => 'required|max:480|unique:what_we_do,description_es,'.$whatwedoToEdit->id,
            'english_description' => 'required|max:480|unique:what_we_do,description_en,'.$whatwedoToEdit->id,
        ]);

        $whatwedoToEdit->description_es = $request->spanish_description;
        $whatwedoToEdit->description_en = $request->english_description;

        $whatwedoToEdit->save();
        return redirect('whatwedo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $whatwedo = Whatwedo::find($id);
        $whatwedo->delete();

        return response("Element deleted.", Response::HTTP_OK);
    }
}
