<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <style>
        .container{
            text-align: center;
            font-size: 14px;
        }

        .message-container{
            width: 60%;
            display: inline-block;
        }

        .header, td.fixed{
            background-color: #051D49;
            color: #ffffff;
            border: 1px solid #9CABCA;
        }

        .header h2{
            text-align: center;
        }

        table, .container{
            width: 100%;
        }

        table{
            border-spacing: 0px;
            border-collapse: collapse;
            border: 1px solid #9CABCA;
        }

        tr{
            border: 1px solid #9CABCA;
        }

        td{
            text-align: left;
        }

        td.fixed{
            width: 100px;
        }

        td{
            vertical-align: top;
            padding: 10px;
        }

        td p{
            margin-top: 0px;
            text-align: justify;
        }
    </style>
</head>


<body>
<div class="container">
    <div class="message-container">
        <table>
            <tr class="header">
                <td colspan="2"><h2>Contact message sent from MOVERSO</h2></td>
            </tr>
            <tr>
                <td class="fixed"><strong>From:</strong></td>
                <td>{{$name}}</td>
            </tr>
            <tr>
                <td class="fixed"><strong>Email:</strong></td>
                <td>{{$email}}</td>
            </tr>
            <tr>
                <td class="fixed"><strong>Phone:</strong></td>
                <td>{{$phone}}</td>
            </tr>
            <tr>
                <td class="fixed"><strong>Subject:</strong></td>
                <td>{{$subject}}</td>
            </tr>
            <tr>
                <td class="fixed"><strong>Message:</strong></td>
                <td>
                    <p>{{$menssage}}</p>
                </td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>