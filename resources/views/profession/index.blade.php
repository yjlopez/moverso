@extends('layouts.app')

@section('content')
    <div class="container news-page">
        <div class="row">
            <div class="col-md-12">
                <div class="table-header">
                    <h4>Managing professions</h4>
                    <a href="{{url('/profession/create')}}">
                        <button type="button" class="btn btn-sm btn-default new-button">Add new</button>
                    </a>
                </div>
                <p id="message-profession"></p>
                <div class="table-responsive">
                    <div class="panel-body">
                        <table id="table_profession" class="table" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Spanish Profession</th>
                                <th>English Profession</th>
                                <th>Actions</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $counter = 0; if($professions == '[]')$profession = new App\Profession();?>
                            @foreach($professions as $profession)
                                <tr id="{{$profession->id}}">
                                    <td><?php echo ++$counter;?></td>
                                    <td>{{$profession->profession_es}}</td>
                                    <td>{{$profession->profession_en}}</td>
                                    <td class="actions">
                                        <a href="/profession/{{$profession->id}}/edit" class="btn btn-xs btn-success">Edit</a>
                                    </td>
                                    <td class="actions">
                                        <button type="button" data-target="#confirmation-message-delete-profession" data-toggle="modal" class="btn btn-xs btn-danger" data-ident="{{$profession->id}}">Delete</button>
                                        {{--<a href="#confirmation-message-delete" role="button" data-toggle="modal" data-ident="{{$news->id}}" class="btn btn-xs btn-danger">Delete</a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal: confirmation message-->
    <div class="modal fade" id="confirmation-message-delete-profession" tabindex="-1" role="dialog" aria-labelledby="myModalLabelprofessionD">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabelprofessionD">Confirmation message</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this element?</p>
                </div>
                <div class="modal-footer">
                    {{csrf_field()}}
                    {{method_field('delete')}}
                    <button type="button" class="btn btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-sm button-cancel" id="delete-profession-confirm">Delete</button>
                </div>
            </div>
        </div>
    </div>

@endsection
