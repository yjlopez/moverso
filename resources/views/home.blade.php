<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Moverso Films') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('css/home.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive-home.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>


<body>
<div class="aux"></div>

<div class="container-fluid site-wrapper">
    <div class="row">
        <?php $generalConfiguration = \App\Configuration::find(1);?>
        @if($generalConfiguration == NULL)
            <p class="notice">There haven't being set any general configuration.</p>
        @else
            <div class="pimary-section col-sm-12 no-padding background">
                @if($generalConfiguration->configuration == 'default')
                    <div id="background1">
                        <div class="star first-stars stars1" data-parallaxify-range="55"></div>
                        <div class="star second-stars stars2" data-parallaxify-range="88"></div>
                        <div class="star first-stars stars3" data-parallaxify-range="120"></div>
                        <div class="star second-stars stars4" data-parallaxify-range="43"></div>
                        <div class="star first-stars stars5" id="stars5"></div>
                    </div>
                @endif
                <div class="row">
                    <div id="main-view" class="col-md-12" style="<?php /*if($generalConfiguration->configuration == 'default') echo 'position: absolute;';*/?>">
                        <div class="cover-container col-xs-12">
                            <img src="{{url('img/home/logoBig.png')}}" alt="moverso film" class="title-image">
                            <h1 class="cover-heading">UNIVERSE IN MOTION</h1>
                            <div class="col-xs-12 main-buttons">
                                <a href="#" class="btn btn-lg btn-default see-our-work" id="see-our-work">SEE OUR WORK</a>
                                <a href="#see-video-modal" role="button" data-toggle="modal" class="btn btn-lg btn-default more-about-us" data-url="{{$generalConfiguration->seew_video_link}}" onclick="loadVideoModal()">MORE ABOUT US</a>
                            </div>
                            <div class="col-xs-12 text-center">
                                <p class="rights">© 2017 · www.moverso.com · All rights reserved.</p>
                            </div>
                        </div>
                    </div>
                </div>
                @if($generalConfiguration->configuration == 'video')
                    <video id="my-video" class="video" muted loop autoplay width="100%" height="">
                        <source src="{{$generalConfiguration->video_url}}" type="video/mp4">
                        <source src="{{$generalConfiguration->video_url}}" type="video/ogg">
                        <source src="{{$generalConfiguration->video_url}}" type="video/webm">
                    </video>
                @endif
            </div>
            <div id="orange-container"></div>
                <nav class="navbar navbar-default">
                    <div class="menu">
                        <p>MENU</p>
                        <img src="{{url('img/home/menu.png')}}" alt="">
                    </div>
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#">
                                <img src="{{asset('img/logo.png')}}" class="logo">
                            </a>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                            <div class="menu-header visible-mob-tab">
                                <img src="{{asset('img/home/menu-mobile-logo.png')}}" class="visible-mob-tab menu-icon">
                                <h1><strong>ME</strong>NU</h1>
                            </div>
                            <ul class="nav navbar-nav navbar-right unactive">
                                <li><a class="smoothScroll" href="#main-view">Home<div class="orange-line"></div></a></li>
                                <li><a class="smoothScroll" href="#recent-work">Recent work<div class="orange-line"></div></a></li>
                                <li><a class="smoothScroll" href="#portfolio">Portfolio<div class="orange-line"></div></a></li>
                                <li><a class="smoothScroll" href="#services">Services<div class="orange-line"></div></a></li>
                                <li><a class="smoothScroll" href="#our-team">Team<div class="orange-line"></div></a></li>
                                <li><a class="smoothScroll" href="#contact-us">Contacts<div class="orange-line"></div></a></li>
                            </ul>
                        </div><!--/.nav-collapse -->
                    </div><!--/.container-fluid -->
                    <div class="gray-line unactive"></div>
                </nav>
        @endif
    </div>

    <div class="row unactive">
        <div id="what-we-do" class="col-md-12">
            <h1>WHAT <strong>WE DO</strong></h1>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: center; padding-right: 0;">
                    @if($generalConfiguration->welcome_image == 1)
                        <img class="welcome-image visible-desktop" src="{{asset('img/home/welcome-image.png')}}" alt="">
                    @endif
                    <div class="wwd-inner-container wwd1">
                        <ul class="whatwedo-filter">
                            <li class="video-category col-xs-3">
                                <p id="video-category">@lang('whatwedo.video')</p>
                                <div class="orange-line"></div>
                            </li>
                            <li class="col-xs-3">
                                <p id="production-category">@lang('whatwedo.production')</p>
                                <div class="orange-line"></div>
                            </li>
                            <li class="col-xs-3">
                                <p id="photography-category">@lang('whatwedo.photography')</p>
                                <div class="orange-line"></div>
                            </li>
                            <li class="col-xs-3">
                                <p id="graphic-category">@lang('whatwedo.graphic_design')</p>
                                <div class="orange-line"></div>
                            </li>
                        </ul>
                        @if($generalConfiguration->welcome_image == 1)
                            <img class="welcome-image visible-mob-tab" src="{{asset('img/home/welcome-image.png')}}" alt="">
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 middle-block" style="text-align: center; padding-right: 0;">
                    <?php
                    $whatwedos = DB::table('what_we_do')->get()->first();?>
                    @if($whatwedos != '[]')
                        <div class="whatwedo-text wwd-inner-container wwd2">
                            <p class="">
                                <?php
                                $locale = App::getLocale();
                                if($locale == 'es'){
                                    echo $whatwedos->description_es;
                                }else{
                                    echo $whatwedos->description_en;
                                }
                                ?>
                            </p>
                        </div>
                    @else
                        <p class="notice">There is no information registered for this section</p>
                    @endif
                </div>
            </div>

        </div>
    </div>




    <div class="row unactive">
        <div id="recent-work" class="col-sm-12">
            <div class="recent-work-header col-sm-12">
                <img src="{{asset('img/home/recent-work-play.png')}}" alt="">
                <h1><strong>RECENT</strong> WORK</h1>
            </div>
            <?php $videos = \App\Video::all(); $count = 0;?>
            @if($videos == '[]')
                <p class="notice">There is no video registered</p>
            @else
                <div class="video-slides col-sm-12">
                    <div id="videoCarousel" class="carousel slide carousel-fade" data-ride="carousel">
                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            @foreach($videos as $video)
                                <?php $videophotos = \App\Videophoto::where('video_id', $video->id)->first();?>
                                @if($video->priority != Null && $video->priority <= 6)
                                        <div class="item <?php if($count == 0) echo 'active';?>">
                                            <div class="videos-header col-md-12 first-one">
                                                <div class="column col-md-4 col-sm-4 col-xs-12 video-title">
                                                    {{$video->title}}
                                                </div>
                                                <div class="column col-md-4 col-sm-4 col-xs-12 title">
                                                    <p>{{$video->artists}}</p>
                                                    <div class="orange-line"></div>
                                                </div>
                                                <div class="column col-md-4 col-sm-4 playing-options">
                                                    <ul>
                                                        <li>
                                                            <a href="<?php if($video->video_url != ''){echo '#see-video-modal';}?>" role="button" data-toggle="modal" data-url="{{$video->video_url}}" class="<?php if($video->video_url == ''){echo 'off-text';}?>" onclick="<?php if($video->video_url != ''){echo 'loadVideoModal()';}?>">PLAY VIDEO</a>
                                                            <div class="<?php if($video->video_url == ''){echo 'off-line';}?> orange-line"></div>
                                                        </li>
                                                        <li>
                                                            <a href="<?php if($video->video_url != ''){echo '#see-video-modal';}?>" role="button" data-toggle="modal" data-url="{{$video->making_off_url}}" class="<?php if($video->video_url == ''){echo 'off-text';}?>" onclick="<?php if($video->video_url != ''){echo 'loadVideoModal()';}?>">MAKING OFF</a>
                                                            <div class="<?php if($video->video_url == ''){echo 'off-line';}?> orange-line"></div>
                                                        </li>
                                                        <li>
                                                            <a href="<?php if($videophotos != Null){echo '#see-image-gallery';}?>" class="<?php if($videophotos == Null){echo 'off-text';}?>" role="button" data-toggle="modal" onclick="fun_video_photos('{{$video->id}}')">PHOTOS</a>
                                                            <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('/photosofavideo')}}">
                                                            <div class="<?php if($videophotos == Null){echo 'off-line';}?>  orange-line"></div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                            <div class="main-content">
                                                {{--<div class="carousel-caption">
                                                    <div class="title-bordered">
                                                        <h1>{{$video->title}}</h1>
                                                    </div>
                                                </div>--}}
                                                <img class="video-main-image" src="{{asset('/storage/video-photo/'.$videophotos['photo'])}}" alt="{{$video->title}}">
                                            </div>

                                            <div class="videos-header videos-header-xs col-sm-12">
                                                <ul>
                                                    <li>
                                                        <a href="<?php if($video->video_url != ''){echo '#see-video-modal';}?>" role="button" data-toggle="modal" data-url="{{$video->video_url}}" class="<?php if($video->video_url == ''){echo 'off-text';}?>" onclick="<?php if($video->video_url != ''){echo 'loadVideoModal()';}?>">PLAY VIDEO</a>
                                                        <div class="<?php if($video->video_url == ''){echo 'off-line';}?> orange-line"></div>
                                                    </li>
                                                    <li>
                                                        <a href="<?php if($video->video_url != ''){echo '#see-video-modal';}?>" role="button" data-toggle="modal" data-url="{{$video->making_off_url}}" class="<?php if($video->video_url == ''){echo 'off-text';}?>" onclick="<?php if($video->video_url != ''){echo 'loadVideoModal()';}?>">MAKING OFF</a>
                                                        <div class="<?php if($video->video_url == ''){echo 'off-line';}?> orange-line"></div>
                                                    </li>
                                                    <li>
                                                        <a href="<?php if($videophotos != Null){echo '#see-image-gallery';}?>" role="button" data-toggle="modal" onclick="fun_video_photos('{{$video->id}}')">PHOTOS</a>
                                                        <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('/photosofavideo')}}">
                                                        <div class="<?php if($videophotos == Null){echo 'off-line';}?> orange-line"></div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <img src="{{asset('/img/home/share.png')}}" alt="" class="share">
                                            <div class="col-md-2 sharebuttons unactive">
                                                @include('layouts.share', ['url' => $video->video_url,'description' => 'This is really cool video', 'image' => 'http://placehold.it/300x300?text=Cool+link'])
                                            </div>
                                        </div>
                                @endif
                                @if($count == 0)
                                    <?php $count++;?>
                                    @continue;
                                @endif

                            @endforeach
                        </div>
                        <!-- Carousel nav -->

                        @if(count($videos) > 1)
                            <div class="col-md-12 gallery-up">
                                <div class="">
                                    @if(count($videos) > 1)
                                        <a class="carousel-control left" href="#videoCarousel" data-slide="prev">
                                            <img src="{{asset('/img/home/back-angular.png')}}" alt="">
                                            <img class="control-line" src="{{asset('/img/home/orange-line.png')}}" alt="">
                                            <img class="control-line control-down-line" src="{{asset('/img/home/orange-line.png')}}" alt="">
                                        </a>
                                    @endif
                                </div>
                                <div class="">
                                    @if(count($videos) > 1)
                                        <a class="carousel-control right" href="#videoCarousel" data-slide="next">
                                            <img src="{{asset('/img/home/next-angular.png')}}" alt="">
                                            <img class="control-line" src="{{asset('/img/home/orange-line.png')}}" alt="">
                                            <img class="control-line control-down-line" src="{{asset('/img/home/orange-line.png')}}" alt="">
                                        </a>
                                    @endif
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            @endif
        </div>
    </div>




    <div class="row unactive">
        <div id="services" class="col-sm-12 no-padding">
            <div class="services-header col-sm-12">
                <img src="{{asset('img/home/services-header.png')}}" alt="">
                <h1><strong>SER</strong>VICES</h1>
            </div>
            <div class="col-md-12 container-services-list no-padding">
                <ul class="services-list">
                    <li>
                        <div class="service-image"><img src="{{asset('img/home/service-direction.png')}}" alt=""></div>
                        <div class="service-text">@lang('services.direction')</div>
                    </li>
                    <li>
                        <div class="service-image"><img src="{{asset('img/home/service-photography.png')}}" alt=""></div>
                        <div class="service-text">@lang('services.photographhy')</div>
                    </li>
                    <li>
                        <div class="service-image"><img src="{{asset('img/home/service-creativity.png')}}" alt="" class="img-double"></div>
                        <div class="double">@lang('services.creativity')</div>
                    </li>
                    <li>
                        <div class="service-image"><img src="{{asset('img/home/service-graphic-design.png')}}" alt="" class="img-double"></div>
                        <div class="double service-text">@lang('services.graphic')</div>
                    </li>
                    <li>
                        <div class="service-image"><img src="{{asset('img/home/service-casting.png')}}" alt=""></div>
                        <div class="service-text">@lang('services.castings')</div>
                    </li>
                    <li>
                        <div class="service-image"><img src="{{asset('img/home/service-location.png')}}" alt=""></div>
                        <div class="service-text">@lang('services.locations')</div>
                    </li>
                    <li>
                        <div class="service-image"><img src="{{asset('img/home/service-equipments.png')}}" alt=""></div>
                        <div class="service-text">@lang('services.equipments')</div>
                    </li>
                    <li>
                        <div class="service-image"><img src="{{asset('img/home/service-permission.png')}}" alt=""></div>
                        <div class="service-text">@lang('services.permissions')</div>
                    </li>
                    <li>

                    </li>
                </ul>
                <a href="#see-video-modal" id="take-a-peak" class="btn btn-lg btn-default" role="button" data-toggle="modal" data-url="{{$generalConfiguration->peak_video_link}}" onclick="loadVideoModal()">TAKE A PEAK</a>
            </div>
        </div>
    </div>


    <div class="row unactive">
        <div id="portfolio" class="col-sm-12">
                <div class="recent-work-header col-sm-12">
                    <img src="{{asset('img/home/portfolio.png')}}" alt="">
                    <h1><strong>PORT</strong>FOLIO</h1>
                </div>
                <div class="portfolio-sub-header col-sm-12">
                    <div class="col-md-2 col-sm-2 col-xs-6 left-block thumbnail-all active-filter">
                        <p class="all-p">@lang('portfolio.all')</p>
                        <div class="orange-line"></div>
                    </div>
                    <div class="col-xs-6 dropdown-image">
                        <img src="{{asset('img/home/down.png')}}" alt="" class="display-thumbnail-filter">
                    </div>
                    <div class="col-md-8 col-sm-12 middle-block hidden-xs hidden-sm col-xs-12">
                        <ul class="whatwedo-filter">
                            <li id="portfolio-direction-category" class=""><p>@lang('portfolio.direction')</p><div class="orange-line"></div></li>
                            <li id="portfolio-photography-category" class=""><p>@lang('portfolio.photography')</p><div class="orange-line"></div></li>
                            <li id="portfolio-production-category" class=""><p>@lang('portfolio.production')</p><div class="orange-line"></div></li>
                            <li id="portfolio-postproduction-category" class=""><p>@lang('portfolio.postproduction')</p><div class="orange-line"></div></li>
                        </ul>
                    </div>
                    <div class="col-sm-2 right-block"></div>
                </div>


            <div class="row">
                <div id="portfolio-content" class="col-md-12">
                    @if($videos == '[]')
                        <p class="notice">There is no video registered</p>
                    @else
                        <ul class="col-sm-12">
                            @foreach($videos as $video)
                                <input type="hidden" id="video-count" data-count="{{count($videos)}}">
                                <?php
                                $videophoto = Illuminate\Support\Facades\DB::table('video_photo')->where('video_id', $video->id)->first();
                                $tvideophotos = \App\Videophoto::where('video_id', $video->id)->get();
                                $count1 =0;
                                ?>
                                <li class="thumbnail-inner unactive {{$video->category}}" data-id="id-{{$video->id}}">
                                    <div id="" class="carousel slide thumbnailCarousel carousel-fade">
                                        <div class="carousel-inner ">
                                            @foreach($tvideophotos as $tvideophoto)
                                                @if($count1 == 0)
                                                    <div class="active item">
                                                        <img class="mythumbnail" src="{{asset('storage/video-photo/'.$tvideophoto->photo)}}" alt="">
                                                        <?php $count1 =1;?>
                                                    </div>
                                                    @continue
                                                @endif
                                                <div class="item"><img class="mythumbnail" src="{{asset('storage/video-photo/'.$tvideophoto->photo)}}" alt=""></div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="thumbnail-hover col-xs-12 unactive id-{{$video->id}}">
                                        <div class="col-sm-12">
                                            <img src="{{asset('/img/home/share.png')}}" alt="" class="share">
                                            <div class="col-md-2 sharebuttons unactive">
                                                @include('layouts.share', ['url' => $video->video_url,'description' => 'This is really cool video', 'image' => 'http://placehold.it/300x300?text=Cool+link'])
                                            </div>
                                        </div>
                                        <div class="col-sm-12 control">
                                            <ul>
                                                <li>
                                                    <a href="<?php if($video->video_url != ''){echo '#see-video-modal';}?>" role="button" data-toggle="modal" data-url="{{$video->making_off_url}}" class="<?php if($video->video_url == ''){echo 'off-text';}?> links1" onclick="<?php if($video->video_url != ''){echo 'loadVideoModal()';}?>">MAKING OFF</a>
                                                    <div class="<?php if($video->video_url == ''){echo 'off-line';} if($video->video_url != ''){echo 'my-line1';}?>  under-line"></div>
                                                </li>
                                                <li>
                                                    <a href="<?php if($video->video_url != ''){echo '#see-video-modal';}?>" role="button" data-toggle="modal" data-url="{{$video->video_url}}" class="<?php if($video->video_url == ''){echo 'off-text';}?>" onclick="<?php if($video->video_url != ''){echo 'loadVideoModal()';}?>"><img class="" src="{{asset('img/home/play-video.png')}}" alt=""><img class="" src="{{asset('img/home/play-video-tablet.png')}}" alt=""></a>
                                                </li>
                                                <li>
                                                    <a href="<?php if($videophoto != Null){echo '#see-image-gallery';}?>" class="<?php if($video->video_url == ''){echo 'off-text';}?> links2" role="button" data-toggle="modal" onclick="fun_video_photos('{{$video->id}}')">PHOTOS</a>
                                                    <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('/photosofavideo')}}">
                                                    <div class="<?php if($videophoto == Null){echo 'off-line';} if($videophoto != Null){echo 'my-line2';}?> under-line"></div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-12 under-header thumb-title">
                                            <p>{{$video->title}}</p>
                                        </div>
                                        <div class="col-sm-12 under-header thumb-title-name">
                                            <p>{{$video->artists}}</p>
                                        </div>
                                    </div>
                                    <div class="orange-line final unactive id-{{$video->id}}"></div>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            </div>
            @if(count($videos) > 9)
                <div class=" col-sm-12 button-container">
                    <button class="btn btn-lg btn-default" id="see-more-thumbnail">@lang('portfolio.see_more')</button>
                </div>
            @endif
        </div>
    </div>




    <div class="row unactive">
        <div id="news" class="col-md-12 no-padding">
            <div class="news-slides col-sm-12">
                <?php $newss = \App\News::all(); $countNews1 = 0; $countNews = 0;?>

                @if($newss == '[]')
                    <p class="notice">There is no news registered</p>
                @else
                    <div id="newsCarousel" class="carousel slide carousel-fade">
                        <ol class="carousel-indicators hidden-xs hidden-sm hidden-md">
                            @for($i = 0; $i < count($newss); $i++)
                                @if($countNews1 == 0)
                                    <li data-target="#newsCarousel" data-slide-to="{{$i}}" class="active"></li>
                                    <?php $countNews1++;?>
                                    @continue;
                                @endif
                                <li data-target="#newsCarousel" data-slide-to="{{$i}}"></li>
                            @endfor
                        </ol>
                        <!-- Carousel items -->
                        <div class="carousel-inner news-carousel">
                            @foreach($newss as $news)
                                <div class="item <?php if($countNews == 0) echo 'active';?>" data-created="{{$news->created_at}}" data-premiere="{{$news->premiere_date}}" data-id="{{$news->id}}" data-seemore="{{$news->see_more_video_link}}">
                                    <div class="col-sm-12">
                                        <img src="{{asset('/img/home/share.png')}}" alt="" class="share">
                                        <div class="col-md-2 sharebuttons unactive">
                                            @include('layouts.share', ['url' => $video->video_url,'description' => 'This is really cool video', 'image' => 'http://placehold.it/300x300?text=Cool+link'])
                                        </div>
                                    </div>
                                    <img class="news-img-carousel" src="{{asset('/storage/news-photos/'.$news->photo)}}" alt="{{$news->title}}">
                                    @if($news->video_link != '')
                                        <div class="youtube-image">
                                            <a href="{{$news->video_link}}"><img class="" src="{{asset('img/home/youtube-play.png')}}" alt=""></a>
                                        </div>
                                    @endif
                                    <div class="col-md-12 news-container">
                                        <div class="col-lg-3 col-xs-8 col-md-6 col-md-push-2 news">
                                            <h1 class="visible-lg">News</h1><h1 class="visible-xs visible-sm visible-md">news</h1>
                                            <div class="orange-line"></div>
                                            <p class="col-sm-12 news-title">
                                                @if($locale == 'es')
                                                    {{$news->translate('es')->title}}
                                                @else
                                                    {{$news->translate('en')->title}}
                                                @endif
                                                <span class="visible-lg" style="display: inline-block !important;">- {{$news->singers}}</span>
                                            </p>
                                            <div class="join-elements">
                                                <time class="timeago" datetime="{{$news->created_at}}"></time>
                                                @if($news->owner_tag != "")
                                                    <div class="vertical-line"></div>
                                                    <a href="{{$news->owner_tag_link}}" class="news-author-tag">{{$news->owner_tag}}</a>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-xs-12 col-sm-12 col-md-8 col-md-offset-2 news-paragraph-container">
                                            <div class="news-paragraph">
                                                @if($locale == 'es')
                                                    {{$news->translate('es')->description}}
                                                @else
                                                    {{$news->translate('en')->description}}
                                                @endif
                                            </div>
                                            {{--<div class="col-sm-6 no-padding">
                                                <a href="#see-video-modal" id="see-more-{{$news->id}}" role="button" data-toggle="modal" data-url="{{$news->see_more_video_link}}" onclick="loadVideoModal()">see more »</a>
                                            </div>--}}
                                            {{--@if($news->video_link != '')
                                                <div class="col-sm-6 youtube-link-content">
                                                    youtube
                                                    <div class="vertical-line"></div>
                                                    <a class="youtube-link" href="{{$news->video_link}}" role="button">moverso films\{{$news->title}}</a>
                                                </div>
                                            @endif--}}
                                        </div>
                                        <div class="col-lg-3 col-xs-4 col-md-2 col-md-pull-6 text-center class-{{$news->id}} counter-container">

                                            <div class="counter-content">
                                                <img src="{{asset('img/home/time-left.png')}}" alt="" id="timer-image">
                                                {{--<a href="#see-video-modal" class="unactive" id="play-video-{{$news->id}}" role="button" data-toggle="modal" data-url="{{$news->video_link}}" onclick="loadVideoModal()"><img src="{{asset('img/home/play-video.png')}}" alt=""></a>--}}

                                                <p class="floating-counter" id="counter-{{$news->id}}" data-url="{{asset('img/home/recent-work-play.png')}}"></p>
                                                <span class="days">days left</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if($countNews == 0)
                                    <?php $countNews++;?>
                                    @continue;
                                @endif
                            @endforeach
                        </div>
                        @if(count($newss) > 1)
                            <a class="carousel-control left" href="#newsCarousel" data-slide="prev">
                                <img src="{{asset('/img/home/back-angular.png')}}" alt="">
                                <img class="control-line" src="{{asset('/img/home/orange-line.png')}}" alt="">
                                <img class="control-line control-down-line" src="{{asset('/img/home/orange-line.png')}}" alt="">
                            </a>
                            <a class="carousel-control right" href="#newsCarousel" data-slide="next">
                                <img src="{{asset('/img/home/next-angular.png')}}" alt="">
                                <img class="control-line" src="{{asset('/img/home/orange-line.png')}}" alt="">
                                <img class="control-line control-down-line" src="{{asset('/img/home/orange-line.png')}}" alt="">
                            </a>
                        @endif
                    </div>
                @endif
            </div>
        </div>
    </div>



    <div class="row unactive">
        <div id="personality" class="col-md-12 no-padding">
            <?php $personalities = \App\Personalities::all(); $countPersonalities = 0;?>

            @if($personalities == '[]')
                <p class="notice">There is no personality registered</p>
            @else
                <div class="personality-slides col-md-12 no-padding">
                    <div id="personalityCarousel" class="carousel slide carousel-fade">
                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            @foreach($personalities as $personality)
                                <div class="item <?php if($countPersonalities == 0) echo 'active';?>">
                                    <img class="visible-md visible-lg personality-img normal carousel-fade" src="{{asset('/storage/personalities/'.$personality->photo)}}" alt="{{$personality->name}}">
                                    <img class="visible-xs visible-sm personality-img mobile carousel-fade" src="{{asset('/storage/personalities/'.$personality->photo_mobile)}}" alt="{{$personality->name}}">
                                    <div class="personality-content col-md-4 col-md-offset-1 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0">
                                        <a href="{{$personality->social_network_address}}"><h1 class="personality-name"><strong>{{$personality->name}}</strong> {{$personality->last_name}}</h1></a>
                                        <?php $profession = \App\Profession::where('id', $personality->profession_id)->first();?>
                                        <p class="profession">{{$profession->profession}}</p>
                                        <div class="orange-line"></div>
                                        <div class="comment-paragraph">{{$personality->summary}}</div>
                                        <div class="work">{{$personality->artist_work}}</div>
                                        <div class="work-interpreters">{{$personality->work_interpreters}}</div>
                                    </div>
                                </div>
                                @if($countPersonalities == 0)
                                    <?php $countPersonalities++;?>
                                    @continue;
                                @endif
                            @endforeach
                        </div>
                    @if(count($personalities) > 1)
                        <!-- Carousel nav -->
                            <a class="carousel-control left" href="#personalityCarousel" data-slide="prev">
                                <img src="{{asset('/img/home/back-angular.png')}}" alt="">
                                <img class="control-line" src="{{asset('/img/home/orange-line.png')}}" alt="">
                                <img class="control-line control-down-line" src="{{asset('/img/home/orange-line.png')}}" alt="">
                            </a>
                            <a class="carousel-control right" href="#personalityCarousel" data-slide="next">
                                <img src="{{asset('/img/home/next-angular.png')}}" alt="">
                                <img class="control-line" src="{{asset('/img/home/orange-line.png')}}" alt="">
                                <img class="control-line control-down-line" src="{{asset('/img/home/orange-line.png')}}" alt="">
                            </a>
                        @endif
                    </div>
                </div>
            @endif
        </div>
    </div>



    <div class="row unactive">
        <div id="our-team" class="col-sm-12 no-padding">
            <div class="row">
                <div class="our-team-header col-sm-12">
                    <img src="{{asset('img/home/our-team.png')}}" alt="">
                    <h1><strong>OUR</strong> TEAM</h1>
                </div>
            </div>
            <?php $members = \App\Member::all(); $countMember = 0; $countMember2 = 0;?>
            @if($members == '[]')
                <p class="notice">There is no team member registered</p>
            @else
                <div class="row">
                    <div class="member-slides col-md-12 no-padding">
                        <!--For devices-->
                        <div id="memberCarousel1" class="carousel carousel-fade slide visible-xs hidden-lg visible-sm hidden-md member-four">
                            <div class="carousel-inner">
                                <!-- Carousel items -->
                                <?php $first = true;?>
                                @foreach($members as $member)
                                    <div class="item <?php if($first == true){echo 'active'; $first = false;}?>">
                                        <?php $first = false;?>
                                        <div class="col-xs-12 member">
                                            <div class="col-xs-12 align-right"><img class="img-circle" src="{{asset('/storage/team/'.$member->photo)}}" alt=""></div>
                                            <div class="col-xs-9 perpendicular">
                                                <div class="middle-align">
                                                    <h1><strong>{{$member->name}} {{$member->last_name}}</strong></h1>
                                                    <?php
                                                    $profession = \App\Profession::where('id', $member->profession_id)->first();
                                                    $role = \App\Role::where('id', $member->role_id)->first();
                                                    ?>
                                                    <p class="profession">{{$role->role}}</p>
                                                    <div class="orange-line"></div>
                                                    <p class="otherprof">{{$profession->profession}}</p>
                                                    <div class="otherprof">{{$member->web_address}}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <!-- Carousel nav -->
                            @if(count($members) > 1)
                                <a class="carousel-control left" href="#memberCarousel1" data-slide="prev">
                                    <img src="{{asset('/img/home/back-angular.png')}}" alt="">
                                    <img class="control-line" src="{{asset('/img/home/orange-line.png')}}" alt="">
                                    <img class="control-line control-down-line" src="{{asset('/img/home/orange-line.png')}}" alt="">
                                </a>
                                <a class="carousel-control right" href="#memberCarousel1" data-slide="next">
                                    <img src="{{asset('/img/home/next-angular.png')}}" alt="">
                                    <img class="control-line" src="{{asset('/img/home/orange-line.png')}}" alt="">
                                    <img class="control-line control-down-line" src="{{asset('/img/home/orange-line.png')}}" alt="">
                                </a>
                            @endif
                        </div>



                        <div id="memberCarousel" class="carousel slide hidden-sm visible-md visible-lg hidden-xs member-one">
                            <div class="carousel-inner">
                                <!-- Carousel items -->
                                <?php $var =0; $counter = 1; $first1 = true;?>
                                @foreach($members as $member)
                                    @if($var==0)
                                        <div class="item <?php if($first1 == true){echo 'active'; $first1 = false;}?>">
                                            @endif
                                            <?php $first1 = false;?>
                                            <div class="col-md-6 col-sm-6 member member-<?php if($counter == 1){echo '1';} if($counter % 2 == 0){echo '2';} if($counter % 3 == 0){echo '3';} if($counter % 4 == 0){echo '4';} ?>">
                                                <div class="col-md-6 col-sm-5 align-right"><img class="img-circle" src="{{asset('/storage/team/'.$member->photo)}}" alt=""></div>
                                                <div class="col-md-6 col-sm-7 perpendicular">
                                                    <h1 class="personality-name"><strong>{{$member->name}} {{$member->last_name}}</strong></h1>
                                                    <?php
                                                    $profession = \App\Profession::where('id', $member->profession_id)->first();
                                                    $role = \App\Role::where('id', $member->role_id)->first();
                                                    ?>
                                                    <p class="profession">{{$role->role}}</p>
                                                    <div class="orange-line"></div>
                                                    <p class="otherprof">{{$profession->profession}}</p>
                                                    <div class="otherprof">{{$member->web_address}}</div>
                                                </div>
                                            </div>
                                            <?php  $var=($var+1); $counter=($counter+1);?>
                                            @if($var==4)
                                        </div>
                                        <?php $var=0;?>
                                    @endif
                                @endforeach
                            </div>
                            <!-- Carousel nav -->
                            @if(count($members) > 4)
                                <a class="carousel-control left" href="#memberCarousel" data-slide="prev">
                                    <img src="{{asset('/img/home/back-angular.png')}}" alt="">
                                    <img class="control-line" src="{{asset('/img/home/orange-line.png')}}" alt="">
                                    <img class="control-line control-down-line" src="{{asset('/img/home/orange-line.png')}}" alt="">
                                </a>
                                <a class="carousel-control right" href="#memberCarousel" data-slide="next">
                                    <img src="{{asset('/img/home/next-angular.png')}}" alt="">
                                    <img class="control-line" src="{{asset('/img/home/orange-line.png')}}" alt="">
                                    <img class="control-line control-down-line" src="{{asset('/img/home/orange-line.png')}}" alt="">
                                </a>
                            @endif
                        </div>
                        @endif
                    </div>
                </div>
        </div>
    </div>



    <div class="row unactive">
        <div id="contact-us" class="col-md-12 no-padding">
            <?php $contactuss = \App\ContactusTranslation::all();?>
            <div class="">
                <div class="contact-us-header col-md-12">
                    <img src="{{asset('img/home/contact-us.png')}}" alt="">
                    <h1><strong>CON</strong>TACTS</h1>
                </div>
            </div>

            <div class="">
                <div class="contact-us-container col-md-12">
                    <form action="{{url('contact-us')}}" method="post">
                        {{ csrf_field() }}
                        {{method_field('POST')}}
                        <div class="">

                            <div class="first control-group col-md-6 col-sm-12 col-xs-12">
                                <input type="text" name="user_name" placeholder="@lang('contacts.name')" class="" required>
                                <input type="email" name="user_email" placeholder="@lang('contacts.email')" class="" required>
                                <input type="tel" name="user_phone" placeholder="@lang('contacts.phone')" class="" required>
                            </div>
                            <div class=" second control-group col-md-6 col-sm-12 col-xs-12">
                                {{--<select name="email_subject" id="email-subject" class="" required>
                                    <option value="">SUBJECT</option>
                                    @foreach($contactuss as $contactus)
                                        <option id="" value="{{$contactus->subject}}">{{$contactus->subject}}</option>
                                    @endforeach
                                </select>--}}
                                <input type="text" placeholder="@lang('contacts.subject')" name="email_subject" id="email-subject" readonly>
                                <div class="subject-options unactive">
                                    <ul>
                                        @foreach($contactuss as $contactus)
                                            <li data-value="{{$contactus->subject}}">{{$contactus->subject}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                <input type="text" name="user_message" placeholder="@lang('contacts.message')" class="" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 button-container">
                                <button type="submit" class="btn btn-lg btn-default">@lang('contacts.send_mail')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>


    <div class="row unactive">
        <div id="follow-us" class="col-md-12 no-padding">
            <div class="parallax parallax-window5" data-enllax-ratio=".5" data-enllax-direction="vertical" data-enllax-type="background"></div>
            <div class="parallax parallax-window4" data-enllax-ratio=".4" data-enllax-direction="vertical" data-enllax-type="background"></div>
            <div class="parallax parallax-window3" data-enllax-ratio=".3" data-enllax-direction="vertical" data-enllax-type="background"></div>
            <div class="parallax parallax-window2" data-enllax-ratio=".2" data-enllax-direction="vertical" data-enllax-type="background"></div>
            <div class="parallax parallax-window" ></div>
            {{--<div class="parallax-window5" data-parallax="scroll" data-image-src="img/background/stars4.png"></div>
            <div class="parallax-window4" data-parallax="scroll" data-image-src="img/background/stars3.png"></div>
            <div class="parallax-window3" data-parallax="scroll" data-image-src="img/background/stars2.png"></div>
            <div class="parallax-window2" data-parallax="scroll" data-image-src="img/background/stars1.png"></div>
            <div class="parallax-window" data-parallax="scroll" data-image-src="img/background/starsbackground.png"></div>--}}
            <div class="follow-content">
                <div class="final-title">
                    <h5>FOLLOW US</h5>
                    <div class="orange-line"></div>
                </div>

                <img src="{{asset('img/home/moverso-final-title.png')}}" width="290" height="auto" alt="moverso film" class="title-image">
                <ul>
                    <li><a href=""><img src="{{asset('img/home/facebook.png')}}" alt=""></a></li>
                    <li><a href=""><img src="{{asset('img/home/youtube.png')}}" alt=""></a></li>
                    <li><a href=""><img src="{{asset('img/home/linkedin.png')}}" alt=""></a></li>
                    <li><a href=""><img src="{{asset('img/home/instagram.png')}}" alt=""></a></li>
                </ul>

                <img src="{{asset('img/home/up.png')}}" alt="moverso film" class="up">
                <p>© 2017 · www.moverso.com · All rights reserved.</p>
            </div>

        </div>
    </div>
</div>


<!-- Modal: see images-->
<div class="modal fade" id="see-image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
        </div>
    </div>
</div>


<!-- Modal: see video-->
<div class="modal fade" id="see-video-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            {{--<video controls="controls" src="{{asset('media/demo.mp4')}}">
                Your browser does not support the HTML5 Video element.
            </video>--}}
            <!--Ver si sirve con youtube-->
            <div class="modal-video">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Scripts -->
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/TweenMax.min.js') }}"></script>
<script src="{{asset('js/TimelineLite.min.js') }}"></script>
<script src="{{asset('js/home.js') }}"></script>
<script src="{{asset('js/jquery.timeago.js') }}"></script>
<script src="{{asset('js/jquery.enllax.min.js') }}"></script>


<script>
    $('.parallax').enllax();
</script>
<script>
    var popupSize = {
        width: 780,
        height: 550
    };

    $(document).on('click', '.sharebuttons a', function(e){

        var
                verticalPos = Math.floor(($(window).width() - popupSize.width) / 2),
                horisontalPos = Math.floor(($(window).height() - popupSize.height) / 2);

        var popup = window.open($(this).prop('href'), 'social',
                'width='+popupSize.width+',height='+popupSize.height+
                ',left='+verticalPos+',top='+horisontalPos+
                ',location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1');

        if (popup) {
            popup.focus();
            e.preventDefault();
        }

    });
</script>
<script>
    window.onload = function(){


    };
</script>
<script>
    $(document).ready(function() {
        var movementStrength1 = 15;
        var height1 = movementStrength1 / $(window).height();
        var width1 = movementStrength1 / $(window).width();

        var movementStrength2 = 10;
        var height2 = movementStrength2 / $(window).height();
        var width2 = movementStrength2 / $(window).width();

        var movementStrength3 = 10;
        var height3 = movementStrength3 / $(window).height();
        var width3 = movementStrength3 / $(window).width();

        var movementStrength4 = 15;
        var height4 = movementStrength4 / $(window).height();
        var width4 = movementStrength4 / $(window).width();

        $(".cover-container").mousemove(function(e){
            var pageX = e.pageX - ($(window).width() / 2);
            var pageY = e.pageY - ($(window).height() / 2);
            var newvalueX1 = width1 * pageX * -1 - 15;
            var newvalueY1 = height1 * pageY * -1 - 30;

            var newvalueX2 = width2 * pageX * +1 + 20;
            var newvalueY2 = height2 * pageY * +1 + 40;

            var newvalueX3 = width3 * pageX * +1 + 10;
            var newvalueY3 = height3 * pageY * +1 + 20;

            var newvalueX4 = width4 * pageX * -1 - 5;
            var newvalueY4 = height4 * pageY * -1 - 10;

            TweenMax.to($('.stars1'), 0.2, {
                x: newvalueX1,
                y: newvalueY1,
                z: 0.1, // use if jitter or shaking is really bad
                rotationZ: 0.01, // use if jitter or shaking is really bad
                ease: Power0.easeOut,
                force3D:true,
                delay: 0.1
            });

            TweenMax.to($('.stars2'), 0.2, {
                x: newvalueX2,
                y: newvalueY2,
                z: 0.1, // use if jitter or shaking is really bad
                rotationZ: 0.01, // use if jitter or shaking is really bad
                ease: Power0.easeOut,
                force3D:true,
                delay: 0.1
            });

            TweenMax.to($('.stars3'), 0.2, {
                x: newvalueX3,
                y: newvalueY3,
                z: 0.1, // use if jitter or shaking is really bad
                rotationZ: 0.01, // use if jitter or shaking is really bad
                ease: Power0.easeOut,
                force3D:true,
                delay: 0.1
            });

            TweenMax.to($('.stars4'), 0.2, {
                x: newvalueX4,
                y: newvalueY4,
                z: 0.1, // use if jitter or shaking is really bad
                rotationZ: 0.01, // use if jitter or shaking is really bad
                ease: Power0.easeOut,
                force3D:true,
                delay: 0.1
            });
        });
    });
</script>
</body>
</html>

