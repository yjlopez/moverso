<ul>
    <li>
        <p class="share-first">SHARE</p>
        <div class="orange-line"></div>
    </li>
    <li class="share-elements">
        <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode($url) }}"
           target="_blank">
            {{--<i class="fa fa-facebook-official"></i> --}}facebook
        </a>
        <div class="down-line"></div>
    </li>
    <li class="share-elements">
        <a href="https://twitter.com/intent/tweet?url={{ urlencode($url) }}"
           target="_blank">
            {{--<i class="fa fa-twitter-square"></i>--}} twitter
        </a>
        <div class="down-line"></div>
    </li>
    <li class="share-elements">
        <a href="https://plus.google.com/share?url={{ urlencode($url) }}"
           target="_blank">
            {{--<i class="fa fa-google-plus-square"></i>--}} google+
        </a>
        <div class="down-line"></div>
    </li>
    <li class="share-elements">
        <a href="https://pinterest.com/pin/create/button/?{{
        http_build_query([
            'url' => $url,
            'media' => $image,
            'description' => $description
        ])
        }}" target="_blank">
            {{--<i class="fa fa-pinterest-square"></i>--}} pinterest
        </a>
        <div class="down-line"></div>
    </li>
</ul>



