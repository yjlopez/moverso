@extends('layouts.app')

@section('content')
<div class="container news-page">
    <div class="row">
        <div class="col-md-12">
            <div class="table-header">
                <h4>Managing news</h4>
                <a href="{{url('/news/create')}}">
                    <button type="button" class="btn btn-sm btn-default new-button">Add new</button>
                </a>
                <p id="messagev"></p>
            </div>
            <div class="table-responsive">
                <div class="panel-body">
                    <table id="table_news" class="table" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Spanish Title</th>
                            <th>English Title</th>
                            <th>Interprets</th>
                            <th>Premiere date</th>
                            <th>Publication date</th>
                            <th>"See more" link</th>
                            <th>Owner tag</th>
                            <th>Photo</th>
                            <th>Description</th>
                            <th>Actions</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $counter = 0; if($newss == '[]')$news = new App\News();?>
                        @foreach($newss as $news)
                            <tr id="{{$news->id}}">
                                <td><?php echo ++$counter;?></td>
                                <td>{{$news->translate('es')->title}}</td>
                                <td>{{$news->translate('en')->title}}</td>
                                <td>{{$news->singers}}</td>
                                <td>{{$news->premiere_date}}</td>
                                <td>{{$news->created_at}}</td>
                                <td>{{$news->see_more_video_link}}</td>
                                <td>{{$news->owner_tag}}</td>
                                <td><a href="#see-image" role="button" data-toggle="modal" data-imageurl="{{$news->photo}}" class="btn btn-xs btn-default">See image</a></td>
                                <td>
                                    <a href="#see-description" role="button" data-toggle="modal" data-description="{{$news->translate('es')->description}}" class="btn btn-xs btn-default">See spanish description</a>
                                    <a href="#see-description" role="button" data-toggle="modal" data-description="{{$news->translate('en')->description}}" class="btn btn-xs btn-default">See english description</a>
                                </td>
                                <td class="actions">
                                    <a href="/news/{{$news->id}}/edit" class="btn btn-xs btn-success">Edit</a>
                                </td>
                                <td class="actions">
                                    <button type="button" data-target="#confirmation-message-delete" data-toggle="modal" class="btn btn-xs btn-danger" data-ident="{{$news->id}}">Delete</button>
                                    {{--<a href="#confirmation-message-delete" role="button" data-toggle="modal" data-ident="{{$news->id}}" class="btn btn-xs btn-danger">Delete</a>--}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal: confirmation message-->
<div class="modal fade" id="confirmation-message-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Confirmation message</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this news?</p>
            </div>
            <div class="modal-footer">
                {{csrf_field()}}
                    {{method_field('delete')}}
                    <button type="button" class="btn btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-sm button-cancel" id="delete-confirm">Delete</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal: see description-->
<div class="modal fade" id="see-description" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Selected news description</h4>
            </div>
            <div class="modal-body">
                <p id="news-description-modal"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal: see image-->
<div class="modal fade" id="see-image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="" alt="" class="modal-image">
        </div>
    </div>
</div>



@endsection
