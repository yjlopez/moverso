@extends('layouts.app')


@section('content')
    <div class="row-fluid">
        <div class="col-sm-10 col-sm-offset-1 create-news-form">
            <div class="page-header">
                <h4>Creating a video</h4>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong> @lang('general.error_message')</strong><br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form class="form-inline" action="{{url('/videos')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{method_field('POST')}}
                <div class="control-group">
                    <label for="video-title" class="control-label">Title</label>
                </div>

                <div class="control-group {{ $errors->has('video_title') ? 'has-error' : '' }}">
                    <input name="video_title" id="video-title" class="span12" type="text" value="{{old('video_title')}}" required>
                </div>

                <div class="control-group">
                    <label for="video-artists" class="control-label">Artists</label>
                </div>
                <div class="control-group separate-group {{ $errors->has('video_artists') ? 'has-error' : '' }}">
                    <input name="video_artists" id="video-artists" class="span12" type="text" value="{{old('video_artists')}}" required>
                </div>


                <div class="control-group ">
                    <label for="making-off-url" class="control-label">Making off URL</label>
                </div>
                <div class="control-group {{ $errors->has('making_off_url') ? 'has-error' : '' }}">
                    <input name="making_off_url" class="span12" id="making-off-url" value="{{old('making_off_url')}}" type="url">
                </div>


                <div class="control-group">
                    <label for="video-url" class="control-label">Video URL</label>
                </div>
                <div class="control-group {{ $errors->has('video_url') ? 'has-error' : '' }}">
                    <input name="video_url" id="video-url" class="span12" type="url" value="{{old('video_url')}}">
                </div>


                <div class="control-group separate-group">
                    <div class="col-sm-2">
                        <label for="video-category" class="control-label">Category</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('video_category') ? 'has-error' : '' }}">
                        <select name="video_category" id="video-category" class="span12" required>
                            <option value=""></option>
                            <option value="Direction" <?php if(old('video_category') && old('video_category') == 'Direction') echo 'selected';?>>Direction</option>
                            <option value="Photography" <?php if(old('video_category') && old('video_category') == 'Photography') echo 'selected';?>>Photography</option>
                            <option value="Production" <?php if(old('video_category') && old('video_category') == 'Production') echo 'selected';?>>Production</option>
                            <option value="PostProduction" <?php if(old('video_category') && old('video_category') == 'PostProduction') echo 'selected';?>>PostProduction</option>
                        </select>
                    </div>

                    <div class="col-sm-2 col-sm-offset-2">
                        <label for="video_priority" class="control-label">Priority</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('video_priority') ? 'has-error' : '' }}">
                        <input name="video_priority" id="video-priority" class="span12" type="number" value="{{old('video_priority')}}">
                    </div>
                </div>

                <div class="control-group col-sm-12 my-buttons">
                    <a href="{{url('/videos')}}">
                        <button type="button" class="btn btn-default">Cancel</button>
                    </a>
                    <button type="submit" class="btn btn-success">Create</button>
                </div>
            </form>
        </div>
    </div>
@endsection
