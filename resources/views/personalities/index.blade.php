@extends('layouts.app')

@section('content')
    <div class="container news-page">
        <div class="row">
            <div class="col-md-12">
                <div class="table-header">
                    <h4>Managing personalities</h4>
                    <a href="{{url('personalities/create')}}">
                        <button type="button" class="btn btn-sm btn-default new-button">Add new</button>
                    </a>
                    <p id="message-artist"></p>
                </div>
                <div class="table-responsive">
                    <div class="panel-body">
                        <table id="table_artists" class="table" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Profession</th>
                                <th>Social Network link</th>
                                <th>Worked with moverso in</th>
                                <th>Work interpreters</th>
                                <th>Image</th>
                                <th>Image for Mobile</th>
                                <th>Description</th>
                                <th>Actions</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $counter = 0; if($personalities == '[]')$artist = new App\Personalities();?>
                            @foreach($personalities as $personality)
                                <tr id="{{$personality->id}}">
                                    <td><?php echo ++$counter;?></td>
                                    <td>{{$personality->name}} {{$personality->last_name}}</td>
                                    <td>{{$personality->profession->profession_en}}</td>
                                    <td>{{$personality->social_network_address}}</td>
                                    <td>{{$personality->artist_work}}</td>
                                    <td>{{$personality->work_interpreters}}</td>
                                    <td><a href="#see-artist-image" role="button" data-toggle="modal" data-imageurl="{{$personality->photo}}" class="btn btn-xs btn-default">See image</a></td>
                                    <td><a href="#see-artist-image" role="button" data-toggle="modal" data-imageurl="{{$personality->photo_mobile}}" class="btn btn-xs btn-default">See image</a></td>
                                    <td>
                                        <a href="#see-artist-summary" role="button" data-toggle="modal" data-description="{{$personality->translate('es')->summary}}" class="btn btn-xs btn-default">See spanish summary</a>
                                        <a href="#see-artist-summary" role="button" data-toggle="modal" data-description="{{$personality->translate('en')->summary}}" class="btn btn-xs btn-default">See english summary</a>
                                    </td>
                                    <td class="actions">
                                        <a href="/personalities/{{$personality->id}}/edit" class="btn btn-xs btn-success">Edit</a>
                                    </td>
                                    <td class="actions">
                                        <button type="button" data-target="#confirmation-message-delete-artist" data-toggle="modal" class="btn btn-xs btn-danger" data-ident="{{$personality->id}}">Delete</button>
                                        {{--<a href="#confirmation-message-delete" role="button" data-toggle="modal" data-ident="{{$news->id}}" class="btn btn-xs btn-danger">Delete</a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal: confirmation message-->
    <div class="modal fade" id="confirmation-message-delete-artist" tabindex="-1" role="dialog" aria-labelledby="myModalLabelArtistD">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabelArtist">Confirmation message</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this personality?</p>
                </div>
                <div class="modal-footer">
                    {{csrf_field()}}
                    {{method_field('delete')}}
                    <button type="button" class="btn btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-sm button-cancel" id="delete-artist-confirm">Delete</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal: see description-->
    <div class="modal fade" id="see-artist-summary" tabindex="-1" role="dialog" aria-labelledby="myModalLabelArtistDD">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabelArtistDD">Selected personality summary</h4>
                </div>
                <div class="modal-body">
                    <p id="artist-summary-modal"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal: see image-->
    <div class="modal fade" id="see-artist-image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <img src="" alt="" class="modal-image">
            </div>
        </div>
    </div>



@endsection
