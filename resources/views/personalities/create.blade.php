@extends('layouts.app')


@section('content')
    <div class="row-fluid">
        <div class="col-sm-10 col-sm-offset-1 create-news-form">
            <div class="page-header">
                <h4>Creating a personality</h4>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong> @lang('general.error_message')</strong><br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form id="personality-form" class="form-inline" action="{{url('personalities')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{method_field('POST')}}
                <div class="control-group separate-group">
                    <div class="col-sm-2">
                        <label for="artist-name" class="control-label">Name</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('artist_name') ? 'has-error' : '' }}">
                        <input name="artist_name" id="artist-name" class="span12" type="text" value="{{old('artist_name')}}" required>
                    </div>
                    <div class="col-sm-2 col-sm-offset-2">
                        <label for="artist-last-name" class="control-label">Last Name</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('artist_last_name') ? 'has-error' : '' }}">
                        <input name="artist_last_name" id="artist-last-name" class="span12" type="text" value="{{old('artist_last_name')}}" required>
                    </div>
                </div>


                <div class="separate-group">
                    <div class="col-sm-2">
                        <label for="artist-profession" class="control-label">Profession</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('artist_profession') ? 'has-error' : '' }}">
                        <select name="artist_profession" id="artist-profession" class="span12">
                            <option value=""></option>
                            @foreach($professions as $profession)
                                <option value="{{$profession->id}}" <?php if(old('artist_profession') == $profession->id) echo 'selected';?>>{{$profession->profession_en}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-sm-2 col-sm-offset-2">
                        <label for="artist-photo" class="control-label">Photo</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('artist_photo') ? 'has-error' : '' }}">
                        <div class="upload">
                            <a href="#" class="btn btn-default span12" role="button">
                                <span class="glyphicon glyphicon-upload"></span>Select an image.
                            </a>
                        </div>
                        <input name="artist_photo" id="artist-photo" class="span12" type="file" value="{{old('artist_photo')}}" size="2048" required>
                    </div>
                </div>

                <div class="separate-group control-group sol-sm-12">
                    <div class="col-sm-2">
                        <label for="artist-web-address" class="control-label">Social Network link</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('artist_web_address') ? 'has-error' : '' }}">
                        <input name="artist_web_address" id="artist-web-address" class="span12" type="text" value="{{old('artist_web_address')}}">
                    </div>
                    <div class="col-sm-2 col-sm-offset-2">
                        <label for="artist-mobile-photo" class="control-label">Photo for  mobile</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('artist_mobile_photo') ? 'has-error' : '' }}">
                        <div class="upload">
                            <a href="#" class="btn btn-default span12" role="button">
                                <span class="glyphicon glyphicon-upload"></span>Select an image.
                            </a>
                        </div>
                        <input name="artist_mobile_photo" id="artist-mobile-photo" class="span12" type="file" value="{{old('artist_mobile_photo')}}" size="2048" required>
                    </div>
                </div>

                <div class="separate-group control-group sol-sm-12">
                    <div class="col-sm-2">
                        <label for="work-interpreters" class="control-label">Work Interpreters</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('work_interpreters') ? 'has-error' : '' }}">
                        <input name="work_interpreters" id="work-interpreters" class="span12" type="text" value="{{old('work_interpreters')}}" required>
                    </div>
                    <div class="col-sm-2 col-sm-offset-2">
                        <label for="artist-work" class="control-label">Worked with moverso in</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('artist_work') ? 'has-error' : '' }}">
                        <input name="artist_work" id="artist-work" class="span12" type="text" value="{{old('artist_work')}}" required>
                    </div>
                </div>


                <div class="control-group separate-group col-sm-12">
                    <label for="artist-summary" class="control-label">Spanish Summary</label>
                </div>
                <div class="control-group col-sm-12 separate-group {{ $errors->has('artist_summary') ? 'has-error' : '' }}">
                    <textarea id="artist-summary" name="artist_summary" rows="3" class="span12" required>{{old('artist_summary')}}</textarea>
                    <p class="characters"></p>
                </div>


                <div class="control-group separate-group col-sm-12">
                    <label for="artist-summary-en" class="control-label">English Summary</label>
                </div>
                <div class="control-group col-sm-12 separate-group {{ $errors->has('artist_summary_en') ? 'has-error' : '' }}">
                    <textarea id="artist-summary-en" name="artist_summary_en" rows="3" class="span12" required>{{old('artist_summary_en')}}</textarea>
                    <p class="charactersPers"></p>
                </div>

                <div class="control-group col-sm-12 my-buttons">
                    <a href="{{url('personalities')}}">
                        <button type="button" class="btn btn-default">Cancel</button>
                    </a>
                    <button type="submit" class="btn btn-success">Create</button>
                </div>
            </form>
        </div>
    </div>
@endsection
