@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col col-md-6 col-md-offset-3"   >
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">@lang('general.contact_form')</h3></div>
                    <div class="panel-body">
                        <form action="{{route('send')}}" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="email">@lang('contacts.email')</label>
                                <input type="email" id="email" name="email" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="subject">@lang('contacts.subject')</label>
                                <input type="text" id="subject" name="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="message">@lang('contacts.message')</label>
                                <textarea name="message" id="message" class="form-control" cols="30" rows="10"></textarea>
                            </div>
                            <div class="form-control">
                                <input type="submit" class="btn btn-success" value="@lang('contacts.send_mail')">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection