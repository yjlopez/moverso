@extends('layouts.app')


@section('content')
    <div class="row-fluid">
        <div class="col-sm-10 col-sm-offset-1 create-news-form">
            <div class="page-header">
                <h4>Creating a role</h4>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong> @lang('general.error_message')</strong><br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form class="form-inline" action="{{url('/roles')}}" method="post">
                {{ csrf_field() }}
                {{method_field('POST')}}

                <div class="control-group separate-group">
                    <div class="col-sm-2">
                        <label for="role" class="control-label">Spanish Role</label>
                    </div>
                    <div class="col-sm-2 {{ $errors->has('spanish_role') ? 'has-error' : '' }}">
                        <input name="spanish_role" id="role" class="span12" type="text" value="{{old('spanish_role')}}" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);" required>
                    </div>
                    <div class="col-sm-2 col-sm-offset-2">
                        <label for="role-en" class="control-label">English Role</label>
                    </div>
                    <div class="col-sm-2 {{ $errors->has('english_role') ? 'has-error' : '' }}">
                        <input name="english_role" id="role-en" class="span12" type="text" value="{{old('english_role')}}" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);" required>
                    </div>
                </div>

                <div class="control-group col-sm-12 my-buttons">
                    <a href="{{url('/roles')}}">
                        <button type="button" class="btn btn-default">Cancel</button>
                    </a>
                    <button type="submit" class="btn btn-success">Create</button>
                </div>
            </form>
        </div>
    </div>
@endsection
