<?php
/**
 * Created by PhpStorm.
 * User: Yania
 * Date: 6/28/2017
 * Time: 2:38 PM
 */

return [
    'name' => 'NOMBRE',
    'email' => 'CORREO',
    'phone' => 'TELÉFONO',
    'subject' => 'ASUNTO',
    'message' => 'MENSAJE',
    'send_mail' => 'ENVIAR EMAIL',
];