<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'El atributo :attribute Debe ser aceptado.',
    'active_url'           => 'El atributo :attribute No es una URL válida.',
    'after'                => 'El atributo :attribute Debe ser una fecha posterior a :date.',
    'after_or_equal'       => 'El atributo :attribute Debe ser una fecha posterior o igual a :date.',
    'alpha'                => 'El atributo :attribute Sólo puede contener letras.',
    'alpha_dash'           => 'El atributo :attribute Sólo puede contener letras, números y guiones.',
    'alpha_num'            => 'El atributo :attribute Sólo puede contener letras y números.',
    'array'                => 'El atributo :attribute Debe ser un arreglo.',
    'before'               => 'El atributo :attribute Debe ser una fecha antes de :date.',
    'before_or_equal'      => 'El atributo :attribute Debe ser una fecha anterior o igual a :date.',
    'between'              => [
        'numeric' => 'El atributo :attribute debe estar entre :min y :max.',
        'file'    => 'El atributo :attribute debe estar entre :min y :max kilobytes.',
        'string'  => 'El atributo :attribute debe estar entre :min y :max caracteres.',
        'array'   => 'El atributo :attribute debe estar entre :min y :max elementos.',
    ],
    'boolean'              => 'El atributo :attribute debe ser verdadero o falso.',
    'confirmed'            => 'La confirmación atributo :attribute no coincide.',
    'date'                 => 'El atributo :attribute no es una fecha válida.',
    'date_format'          => 'El atributo :attribute no coincide con el formato :format.',
    'different'            => 'El atributo :attribute y :other deben ser diferentes.',
    'digits'               => 'El atributo :attribute debe ser :digits dígitos.',
    'digits_between'       => 'El atributo :attribute debe estar entre :min y :max dígitos.',
    'dimensions'           => 'El atributo :attribute tiene dimensiones no válidas de la imagen.',
    'distinct'             => 'El atributo :attribute field tiene un valor duplicado.',
    'email'                => 'El atributo :attribute debe ser una dirección de correo.',
    'exists'               => 'El atributo selected :attribute no es válido.',
    'file'                 => 'El atributo :attribute debe ser un archivo.',
    'filled'               => 'El atributo :attribute debe tener un valor.',
    'image'                => 'El atributo :attribute debe ser una imagen.',
    'in'                   => 'El atributo seleccionado :attribute no es válido.',
    'in_array'             => 'El atributo :attribute no existe en :other.',
    'integer'              => 'El atributo :attribute debe ser un entero.',
    'ip'                   => 'El atributo :attribute debe ser una dirección IP válida.',
    'json'                 => 'El atributo :attribute debe ser un JSON válidos',
    'max'                  => [
        'numeric' => 'El atributo :attribute no debe ser mayor que :max.',
        'file'    => 'El atributo :attribute no debe ser mayor que :max kilobytes.',
        'string'  => 'El atributo :attribute no debe ser mayor que :max caracteres.',
        'array'   => 'El atributo :attribute no debe ser mayor que :max elementos.',
    ],
    'mimes'                => 'El atributo :attribute debe ser un archivo de tipo: :values.',
    'mimetypes'            => 'El atributo :attribute debe ser un archivo de tipo: :values.',
    'min'                  => [
        'numeric' => 'El atributo :attribute debe ser de al menos :min.',
        'file'    => 'El atributo :attribute debe ser de al menos :min kilobytes.',
        'string'  => 'El atributo :attribute debe ser de al menos :min caracteres.',
        'array'   => 'El atributo :attribute debe tener al menos :min elementos.',
    ],
    'not_in'               => 'El :attribute seleccionado no es válido.',
    'numeric'              => 'El atributo :attribute debe ser un número.',
    'present'              => 'El atributo :attribute debe estar presente.',
    'regex'                => 'El atributo :attribute contiene un formato no válido.',
    'required'             => 'El atributo :attribute  es requerido.',
    'required_if'          => 'El atributo :attribute  es requerido cuando :other es :value.',
    'required_unless'      => 'El atributo :attribute  es requerido a menos que :other is in :values.',
    'required_with'        => 'El atributo :attribute  es requerido cuando :values está presente.',
    'required_with_all'    => 'El atributo :attribute  es requerido cuando :values está presente.',
    'required_without'     => 'El atributo :attribute  es requerido cuando :values no está presente.',
    'required_without_all' => 'El atributo :attribute es requerido cuando ninguno de los valores :values están presentes.',
    'same'                 => 'El atributo :attribute y :other deben coincidir.',
    'size'                 => [
        'numeric' => 'El atributo :attribute debe ser de :size.',
        'file'    => 'El atributo :attribute debe ser de :size kilobytes.',
        'string'  => 'El atributo :attribute debe ser de :size caracteres.',
        'array'   => 'El atributo :attribute debe contener :size elementos.',
    ],
    'string'               => 'El atributo :attribute debe ser una cadena.',
    'timezone'             => 'El atributo :attribute debe ser una zona válida.',
    'unique'               => 'El atributo :attribute ya ha sido utilizado.',
    'uploaded'             => 'El atributo :attribute no se pudo cargar.',
    'url'                  => 'El atributo :attribute contiene un formato no válido.',
    'unique_two_elements'   =>  'Los valores del nombre y el apellido introducidos ya existen.',
    'unique_two_elements_edit'   =>  'Los valores del nombre y el apellido introducidos ya existen.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
