<?php
/**
 * Created by PhpStorm.
 * User: Yania
 * Date: 6/28/2017
 * Time: 12:45 PM
 */

return [
    'video' => 'VIDEOS',
    'production' => 'PRODUCCIONES',
    'photography' => 'FOTOGRAFÍA',
    'graphic_design' => 'DISEÑO GRÁFICO',
    'what' => 'QUE',
    'we_do' => 'HACEMOS',
];
