<?php
/**
 * Created by PhpStorm.
 * User: Yania
 * Date: 6/30/2017
 * Time: 10:14 AM
 */

return [
    'home' => 'HOME',
    'recent_work' => 'RECENT WORK',
    'portfolio' => 'PORTFOLIO',
    'services' => 'SERVICES',
    'team' => 'TEAM',
    'contacts' => 'CONTACTS',
];