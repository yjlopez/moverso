<?php
/**
 * Created by PhpStorm.
 * User: Yania
 * Date: 6/28/2017
 * Time: 2:13 PM
 */

return [
    'share' => 'SHARE',
    'covering_title' => 'UNIVERSE IN MOTION',
    'see_work_button' => 'SEE OUR WORK',
    'more_about_us_button' => 'MORE ABOUT US',
    'error_message' => 'There were some problems with your input.',
    'contact_form' => 'Contact form',
    'attention' => 'Attention!!!',
    'message_send' => 'Your message has been sent, we will respond to your request soon.',
];